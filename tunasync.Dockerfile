FROM ubuntu:latest AS tunasync-manager

WORKDIR /work

RUN apt-get update \
  && apt-get install -y \
  curl \
  git \
  && curl -sSL https://github.com/tuna/tunasync/releases/download/v0.8.0/tunasync-linux-amd64-bin.tar.gz -o /tmp/tunasync.tar.gz \
  && tar -xzvf /tmp/tunasync.tar.gz \
  && mv tunasync /usr/bin/tunasync \
  && chmod +x /usr/bin/tunasync \
  && mv tunasynctl /usr/bin/tunasynctl \
  && chmod +x /usr/bin/tunasynctl \
  && groupadd -g 2001 mirrorgroup \
  && useradd -u 2101 -g mirrorgroup mirrors \
  && chown -Rf mirrors:mirrorgroup /mirrors \
  && chmod 755 /mirrors \
  && mkdir -p /srv/tunasync/conf /srv/tunasync/db /srv/tunasync/log/tunasync \
  && git clone https://github.com/tuna/tunasync-scripts.git /srv/tunasync/scripts \
  && git clone https://gitlab.com/megabyte-labs/docker/docker-compose/seafood-restaurant.git /usr/src/seafood-restaurant \
  && chmod +x /usr/src/seafood-restaurant/local/tunasync-manager \
  && ln -s /usr/src/seafood-restaurant/local/tunasync-manager /usr/bin/tunasync-manager \
  && chmod +x /usr/src/seafood-restaurant/local/tunasync-worker \
  && ln -s /usr/src/seafood-restaurant/local/tunasync-worker /usr/bin/tunasync-worker \
  && ln -s /usr/src/seafood-restaurant/local/manager.conf /srv/tunasync/conf/manager.conf \
  && ln -s /usr/src/seafood-restaurant/local/worker.conf /srv/tunasync/conf/worker.conf

VOLUME /mirrors
VOLUME /srv/tunasync/db

ENTRYPOINT ["tunasync-manager"]

FROM tunasync-manager AS tunasync-worker

ENTRYPOINT ["tunasync-worker"]

FROM ubuntu:latest AS tunasync-mirror-web

WORKDIR /work

RUN apt-get update \
  && apt-get install -y \
  curl \
  git \
  && git clone https://github.com/tuna/mirror-web.git \
  && cd mirror-web \
  && docker build -t tunasync-mirror-web-build -f Dockerfile.build . \
  && docker run -it --rm -v $PWD:/data tunasync-mirror-web-build \
  && curl -sSL https://mirrors.tuna.tsinghua.edu.cn/static/tunasync.json > static/tunasync.json \
  && curl -sSL https://mirrors.tuna.tsinghua.edu.cn/static/tunet.json > static/tunet.json \
  && mkdir -p static/status \
  && curl -sSL https://mirrors.tuna.tsinghua.edu.cn/static/status/isoinfo.json > static/status/isoinfo.json

RUN jekyll build

FROM nginx:latest AS seafood-server

COPY --from=tunasync-mirror-web /work/mirror-web /usr/share/nginx/html
